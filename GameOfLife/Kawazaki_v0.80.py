from tkinter import *
from numpy import *
from random import randint
from random import random
from copy import copy

class Application(Frame):
    "A GUI application with 1 click-counting button"
    def __init__(self,master):
        "Initialize the Frame"
        Frame.__init__(self,master)
        self.columnconfigure(0, pad = 30)
        self.grid()
        self.count = 0
        self.countbluecells = 0
        self.Nfree = 0 #initiates the number of free particles to zero
        self.Emean =0 #initiates the average energy to zero
        self.E2 = 0 #initiates the average energy squared to zero
        self.create_widgets()
        
    def create_widgets(self):
        "create a frame that contains all the buttons, scale and simulation cells widgets"
        self.frame1 = Frame(self, width = 750, height = 350, borderwidth = 5, relief = "ridge", padx = 5, pady = 5, bg = "white")
        self.frame1.grid(row =0, column = 0)
        for i in range(3):
            self.frame1.columnconfigure(i, pad = 10)
        self.frame_scale = Frame(self.frame1, width = 200, height = 200, borderwidth = 3, relief = "groove", bg = "white")
        for i in range(5):
            self.frame_scale.rowconfigure(i, pad = 35)
        for i in range(2):
            self.frame_scale.columnconfigure(i, pad = 15)
        self.frame_scale.grid(row = 0, column = 1)
        self.frame_buttons = Frame(self.frame1, width = 200, height = 200, borderwidth = 3, relief = "groove", bg = "white")
        for i in range(6):
            self.frame_buttons.rowconfigure(i,pad = 30)
        self.frame_buttons.grid(row = 0, column = 0)
        self.frame_simu = Frame(self.frame1, width = 350, height = 350, borderwidth = 3, relief = "groove", bg = "white")
        self.frame_simu.grid(row = 0, column = 2)
        "create a scale button to choose epsilon"
        self.beta = 0.1
        self.ScaleEnergyVar = IntVar
        self.ScaleEnergy = Scale(self.frame_scale, from_=0, to =5, resolution = 0.1, orient = HORIZONTAL, label = "E/kT", variable = self.ScaleEnergyVar, font =('Helvetica','18'))
        self.ScaleEnergy.grid(row = 2, column = 1)
        self.ScaleEnergy.set(0)
        "create a scale button to choose number of cells"
        self.ScaleNcellsVar = IntVar
        self.ScaleNcells = Scale(self.frame_scale, from_=1, to =5, resolution = 0.2, orient = HORIZONTAL, label = "log(Ncells)", variable = self.ScaleNcellsVar, font =('Helvetica','18'))
        self.ScaleNcells.grid(row = 0, column = 1)
        self.ScaleNcells.set(2)
        "create a scale button to choose number of slices for x-axis entropy"
        self.ScaleNslicesVar = IntVar
        self.ScaleNslices = Scale(self.frame_scale, from_=1, to =15, orient = HORIZONTAL, label = "EntGrid", variable = self.ScaleNcellsVar, font =('Helvetica','18'))
        self.ScaleNslices.grid(row = 1, column = 1)
        self.ScaleNslices.set(2)
        "create a scale button to choose radius of disk created by clicking right button"
        self.ScaleRadiusVar = IntVar
        self.ScaleRadius = Scale(self.frame_scale, from_=1, to =50, orient = HORIZONTAL, label = "DiskRadius", variable = self.ScaleRadiusVar, font =('Helvetica','18'))
        self.ScaleRadius.grid(row = 0,column = 2)
        self.ScaleRadius.set(3)
        "create a scale button to choose speed of kawazaki simulation"
        self.ScaleSpeedVar = IntVar
        self.ScaleSpeed = Scale(self.frame_scale, from_=1, to =20, orient = HORIZONTAL, label = "SimuSpeed", variable = self.ScaleRadiusVar, font =('Helvetica','18'))
        self.ScaleSpeed.grid(row = 1,column = 2)
        self.ScaleSpeed.set(1)
        "create a reset button that resets cells display"
        self.resetcount = 0
        self.Reset = Button(self.frame_buttons, text = "Reset", font =('Helvetica','18'))
        self.Reset["command"] = self.reset_cells #the option "command" means action when clicking on it
        self.Reset.grid(row = 0, column = 0)
        "create a button that starts/stops the kawazaki dynamics"
        self.do_run = False
        self.startclick = True
        self.buttonStartStop = Button(self.frame_buttons, text = "Start", font =('Helvetica','18'), bg = "green")
        self.buttonStartStop["command"] = self.start_stop_simu
        self.buttonStartStop.grid(row = 1, column = 0)
        "create a button that enable/disable the grid"
        self.width = 1
        self.widthButton = Button(self.frame_buttons, text = "Grid On/Off", font =('Helvetica','18'))
        self.widthButton["command"] = self.switch_grid #the option "command" means action when clicking on it
        self.widthButton.grid(row = 2, column = 0)
        "create a button that prints to image file the grid"
        self.PrintGridButton = Button(self.frame_buttons, text = "PrintConfig", font =('Helvetica','18'))
        self.PrintGridButton["command"] = self.print_grid #the option "command" means action when clicking on it
        self.PrintGridButton.grid(row = 3, column = 0)
        "create a button that prints to image file the entropy curves"
        self.PrintEntropyButton = Button(self.frame_buttons, text = "PrintEntropy", font =('Helvetica','18'))
        self.PrintEntropyButton["command"] = self.print_entropy #the option "command" means action when clicking on it
        self.PrintEntropyButton.grid(row = 4, column = 0)
        "create a button that prints to image file the energy curves"
        self.PrintEnergyButton = Button(self.frame_buttons, text = "PrintEnergy", font =('Helvetica','18'))
        self.PrintEnergyButton["command"] = self.print_energy #the option "command" means action when clicking on it
        self.PrintEnergyButton.grid(row = 5, column = 0)
        "create a button that displays the number of blue cells on real time"
        self.buttoncounter = Button(self.frame_simu, text = self.countbluecells, font =('Helvetica','18'))
        self.buttoncounter.grid(row = 0, column = 0)
        "create a big canva to contain the simulation cells"
        self.size = 300
        self.canvas = Canvas(self.frame_simu, width=self.size, height=self.size, bg ="red")
        self.canvas.grid(row = 1, column = 0)
        "create a frame that contains the entropy plot"
        self.frame2 = Frame(self, width = 750, height = 350, borderwidth = 5, relief = "ridge", padx = 40, pady = 63, bg = "white")
        self.frame2.grid(row =0, column = 1)
        "create a circle in a canvas to plot the entropy as a function of time"
        self.z = Canvas(self.frame2, width = 600, height = 300, bg = "white")
        self.z.grid(row = 1, column = 0)
        self.line3 = self.z.create_line(10.,10,10.,250, arrow=FIRST, fill="black", width = 3)
        self.line4 = self.z.create_line(2.,245,590,245, arrow=LAST, fill="black", width = 3)
        self.z.create_text(60,15, text = "Entropy", font =('Helvetica','18'))
        self.z.create_text(550,270, text = "Time", font =('Helvetica','18'))
        self.initialcircle = self.z.create_oval(25,243,30,248, fill="green")
        "create a button that displays the entropy value in real time"
        self.s = 0
        self.entropycounter = Button(self.frame2, text = self.s, font =('Helvetica','18'))
        self.entropycounter.grid(row = 0, column = 0)
        "create a frame that contains the energy plot"
        self.frame3 = Frame(self, width = 750, height = 350, borderwidth = 5, relief = "ridge", padx = 20, pady = 45, bg = "white")
        self.frame3.grid(row =1, column = 0)
        "create a circle in a canvas to plot the energy as a function of time"
        self.zz = Canvas(self.frame3, width = 700, height = 250, bg = "white")
        self.zz.grid(row = 1, column = 0)
        self.energyline3 = self.zz.create_line(10.,10,10.,220, arrow=FIRST, fill="black", width = 3)
        self.energyline4 = self.zz.create_line(2.,215,590,215, arrow=LAST, fill="black", width = 3)
        self.zz.create_text(60,15, text = "Energy", font =('Helvetica','18'))
        self.zz.create_text(550,230, text = "Time", font =('Helvetica','18'))
        self.initialenergycircle = self.zz.create_oval(25,25,30,30, fill="green")
        "create a button that displays the energy value in real time"
        self.energy = 0
        self.energycounter = Button(self.frame3, text = self.energy, font =('Helvetica','18'))
        self.energycounter.grid(row = 0, column = 0)
        "create a frame that contains the probability plot"
        self.frame4 = Frame(self, width = 750, height = 350, borderwidth = 5, relief = "ridge", padx = 40, pady = 83, bg = "white")
        self.frame4.grid(row =1, column = 1)
        "create an array of rectangles framed by axis in a canvas"
        self.nrect = 2
        self.w = Canvas(self.frame4, width = 600, height = 220, bg = "white")
        self.w.grid(row = 3, column = 3)
        self.line1 = self.w.create_line(8.,0,8.,200, arrow=FIRST, fill="black", width = 3)
        self.line2 = self.w.create_line(2.,175,600,175, arrow=LAST, fill="black", width = 3)
        self.w.create_text(70,15, text = "Frequency", font =('Purisa','18'))
        self.w.create_text(570,200, text = "Slice", font =('Purisa','18'))
        self.FreqRect = [0 for x in range(self.nrect)]
        self.Delta = 400*3/(4*self.nrect-1)
        self.T = 4*self.Delta/3
        for x in range(self.nrect):
            self.FreqRect[x] = self.w.create_rectangle(130+x*self.T-self.Delta*0.5, (1-0)*100+70, 130+x*self.T+self.Delta*0.5, 170, fill="yellow")
        "create an array of cells all initiated with the same value"
        self.bluecells = []
        self.nx = 10
        self.ny = self.nx
        self.ntot = self.nx*self.ny
        self.state = [0 for x in range(self.ntot)]
        for x in range(self.ntot):
            self.state[x] = 0
            
            
        def click(event):
            if self.canvas.find_withtag(CURRENT):
                cellID = int(self.canvas.gettags(CURRENT)[0])
                if cellID >= self.ntot:
                    self.canvas.itemconfig(self.rect[cellID], state = HIDDEN)
                    self.canvas.itemconfig(self.rect[cellID-self.ntot], state = NORMAL)
                    self.bluecells.remove(self.canvas.gettags(self.rect[cellID-self.ntot])[0])
                    self.countbluecells-=1
                    self.state[cellID-self.ntot] = 0
                if cellID < self.ntot:
                    self.canvas.itemconfig(self.rect[cellID], state = HIDDEN)
                    self.canvas.itemconfig(self.rect[cellID+self.ntot], state = NORMAL)
                    self.bluecells.append(self.canvas.gettags(self.rect[cellID])[0])
                    self.countbluecells+=1
                    self.state[cellID] = 1
            self.canvas.update_idletasks()
            self.density = 0
            if self.ntot > 0:
                self.density = self.countbluecells/self.ntot
            self.NumberAndDensity = "N = {0}; eta = {1:4.3}".format(self.countbluecells, self.density)
            self.buttoncounter.config(text = self.NumberAndDensity)
            self.static_update_entropy()
            self.compute_energy()
            self.static_update_energy()
            self.update_rect()
                    
        self.NColorlayers = 2
        self.rect = [0 for x in range(self.NColorlayers*self.ntot)]
        
        for i in range(self.ntot):
            self.rectsize = self.size//self.nx
            igrid = i%self.ntot
            self.rect[i] = self.canvas.create_rectangle((igrid%(self.nx))*self.rectsize, self.rectsize*(igrid//self.nx), (igrid%(self.nx))*self.rectsize+self.rectsize, self.rectsize*(igrid//self.nx)+self.rectsize, fill="red", tag = i, width=self.width, state = NORMAL)
            
        for i in range(self.ntot, self.NColorlayers*self.ntot):
            self.rectsize = self.size//self.nx
            igrid = i%self.ntot
            self.rect[i] = self.canvas.create_rectangle((igrid%(self.nx))*self.rectsize, self.rectsize*(igrid//self.nx), (igrid%(self.nx))*self.rectsize+self.rectsize, self.rectsize*(igrid//self.nx)+self.rectsize, fill="blue", tag = i, width=self.width, state = HIDDEN)

        self.canvas.bind("<Button-1>", click)
        
        
        def create_disk(event):
            if self.canvas.find_withtag(CURRENT):   
                cellID = int(self.canvas.gettags(CURRENT)[0])
                self.imax = self.ScaleRadius.get()
                cellIDgrid = cellID%self.ntot
                for i in range(self.imax):
                    jeff = int(ceil(sqrt(self.imax*self.imax-i*i)))
                    for j in range(jeff):
                        cellIDred = (cellIDgrid+i+j*self.nx)%self.ntot
                        if self.state[cellIDred] == 0:
                            cellIDblue = cellIDred+self.ntot
                            self.canvas.itemconfig(self.rect[cellIDred], state = HIDDEN)
                            self.canvas.itemconfig(self.rect[cellIDblue], state = NORMAL)
                            self.bluecells.append(self.canvas.gettags(self.rect[cellIDred])[0])
                            self.state[cellIDred] = 1
                            self.countbluecells += 1
                        cellIDred = cellIDgrid+i-j*self.nx
                        if self.state[cellIDred] == 0:
                            cellIDblue = cellIDred+self.ntot
                            self.canvas.itemconfig(self.rect[cellIDred], state = HIDDEN)
                            self.canvas.itemconfig(self.rect[cellIDblue], state = NORMAL)
                            self.bluecells.append(self.canvas.gettags(self.rect[cellIDred])[0])
                            self.state[cellIDred] = 1
                            self.countbluecells += 1
                        cellIDred = cellIDgrid-i+j*self.nx
                        if self.state[cellIDred] == 0:
                            cellIDblue = cellIDred+self.ntot
                            self.canvas.itemconfig(self.rect[cellIDred], state = HIDDEN)
                            self.canvas.itemconfig(self.rect[cellIDblue], state = NORMAL)
                            self.bluecells.append(self.canvas.gettags(self.rect[cellIDred])[0])
                            self.state[cellIDred] = 1
                            self.countbluecells += 1
                        cellIDred = cellIDgrid-i-j*self.nx
                        if self.state[cellIDred] == 0:
                            cellIDblue = cellIDred+self.ntot
                            self.canvas.itemconfig(self.rect[cellIDred], state = HIDDEN)
                            self.canvas.itemconfig(self.rect[cellIDblue], state = NORMAL)
                            self.bluecells.append(self.canvas.gettags(self.rect[cellIDred])[0])
                            self.state[cellIDred] = 1
                            self.countbluecells += 1
                    
            self.canvas.update_idletasks()
            self.density = 0
            if self.ntot > 0:
                self.density = self.countbluecells/self.ntot
            self.NumberAndDensity = "N = {0}; eta = {1:4.3}".format(self.countbluecells, self.density)
            self.buttoncounter.config(text = self.NumberAndDensity)
            self.static_update_entropy()
            self.compute_energy()
            self.static_update_energy()
            self.update_rect()
            
            
                    
        self.canvas.bind("<Button-2>", create_disk)
    
    def reset_cells(self):
        self.Nfree = 0 #initiates the number of free particles to zero
        self.Emean =0 #initiates the average energy to zero
        self.E2 = 0 #initiates the average energy squared to zero
        if self.count > 0:
            self.resetcount += 1
        self.count = 0
        for x in range(self.ntot):
            self.canvas.delete(self.rect[x])
        "create an array of cells all initiated in the zero state"
        self.nx = int(sqrt(pow(10,self.ScaleNcells.get())))
        self.ntot = self.nx*self.nx
        for i in range(self.countbluecells):
            del self.bluecells[0] #trim the list self.bluecells by one end
        self.countbluecells = 0
        self.state = [0 for x in range(self.ntot)]
        for x in range(self.ntot):
            self.state[x] = 0
            
        self.rect = [0 for x in range(self.NColorlayers*self.ntot)]
        
        for i in range(self.ntot):
            self.rectsize = self.size/self.nx
            igrid = i%self.ntot
            self.rect[i] = self.canvas.create_rectangle((igrid%(self.nx))*self.rectsize, self.rectsize*(igrid//self.nx), (igrid%(self.nx))*self.rectsize+self.rectsize, self.rectsize*(igrid//self.nx)+self.rectsize, fill="red", tag = i, width=self.width, state = NORMAL)
            
        for i in range(self.ntot, self.NColorlayers*self.ntot):
            self.rectsize = self.size/self.nx
            igrid = i%self.ntot
            self.rect[i] = self.canvas.create_rectangle((igrid%(self.nx))*self.rectsize, self.rectsize*(igrid//self.nx), (igrid%(self.nx))*self.rectsize+self.rectsize, self.rectsize*(igrid//self.nx)+self.rectsize, fill="blue", tag = i, width=self.width, state = HIDDEN)
                                                     
        self.buttoncounter.config(text = self.countbluecells)
        self.energy = 0
        self.static_update_energy()
        self.static_update_entropy()
            
    def switch_grid(self):
        self.width = (self.width+1)%2
        for i in range(self.ntot):
            self.canvas.itemconfig(self.rect[i], width = self.width)
        self.canvas.update_idletasks()
    
    def print_grid(self):
        self.canvas.update()
        self.canvas.postscript(file = "Configuration_output.ps")
    
    def print_entropy(self):
        self.z.update()
        self.z.postscript(file = "Entropy_output.ps")
        
    def print_energy(self):
        self.zz.update()
        self.zz.postscript(file = "Energy_output.ps")
        self.density = self.countbluecells/self.ntot
        self.filename = "Energies_N_{0:d}_eta_{1:f}.txt".format(self.countbluecells, self.density)
        f = open(self.filename, 'a')
        #f.write(self.energy + self.beta + self.density  + '\n')
        self.Emean = self.Emean/self.CountForAverage
        self.E2 = self.E2/self.CountForAverage
        self.VarE = self.E2 - self.Emean*self.Emean
        self.Cv = self.beta*self.VarE
        f.write("{:f} \t".format(self.Emean))
        f.write("{:f} \t".format(-self.epsilon*self.beta))
        f.write("{:f} \t".format(self.P))
        f.write("{:f} \n".format(self.Cv/self.countbluecells))
        f.close
    
    def start_stop_simu(self):
        if self.startclick:
            self.buttonStartStop.configure(text = "Stop", bg = "red")
            self.start_simu()
            self.startclick = False
        else :
            self.buttonStartStop.configure(text = "Start", bg = "green")
            self.stop_simu()
            self.startclick = True
    def start_simu(self):
        self.do_run = True
        self.Emean = 0
        self.Nfree = 0
        self.E2 = 0
        self.CountForAverage = 0
        self.run_kawazaki()
    
    def stop_simu(self):
        self.do_run = False
        
    def static_update_energy(self):
        if self.energy < 0:
            self.energydisplay = -log(-self.energy)
        if self.energy == 0:
            self.energydisplay = 0
        "plot a dot at the corresponding location"
        if self.count%1 == 0:
            if self.resetcount == 0:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="green")
            if self.resetcount == 1:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="red")
            if self.resetcount == 2:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="blue")
            if self.resetcount == 3:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="yellow")
            if self.resetcount == 4:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="purple")
            if self.resetcount == 5:
                self.zz.delete(self.initialenergycircle)
                self.initialenergycircle = self.zz.create_oval(25,25-15*(self.energydisplay),30,30-15*(self.energydisplay), fill="orange")
        self.energycounter.configure(text = self.energy)
        
    def update_energy(self):
        if self.energy < 0:
            self.energydisplay = -log(-self.energy)
        if self.energy == 0:
            self.energydisplay = 0
        "plot a dot at the corresponding location"
        if self.count%60 == 0:
            if self.resetcount == 0:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="green")
            if self.resetcount == 1:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="red")
            if self.resetcount == 2:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="blue")
            if self.resetcount == 3:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="yellow")
            if self.resetcount == 4:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="purple")
            if self.resetcount == 5:
                self.energycircle = self.zz.create_oval(25+(self.count*1.)%600,25-15*(self.energydisplay),30+(self.count*1.)%600,30-15*(self.energydisplay), fill="orange")
        self.VarEbuffer = self.E2buffer - self.Emeanbuffer*self.Emeanbuffer
        self.Vfreebuffer = self.ntot-self.countbluecells+self.Nfreebuffer#accessible volume to the free particles
        if self.beta != 0:
            self.P = self.Nfreebuffer/(self.Vfreebuffer*self.beta) #ideal gas pressure for the free particles
            self.energyinfo = "E = {0}, <E> = {1:4.3}, Var(E) = {2:4.3}, P = {3:4.3}".format(self.energy, self.Emeanbuffer, self.VarEbuffer, self.P)
        if self.beta == 0:
            self.energyinfo = "E = {0}, <E> = {1:4.3}, Var(E) = {2:4.3}, P = Inf.".format(self.energy, self.Emeanbuffer, self.VarEbuffer)
        
        self.energycounter.configure(text = self.energyinfo)
        
    def static_update_entropy(self):
        "create an horizontal frequency by splitting the x-axis in n parts"
        self.n = self.ScaleNslices.get()
        self.freq = [0 for x in range(self.n)]
        for x in range(self.n):
            self.freq[x] = 0 
        "calculate the frequency to be in one half of the box"
        for i in range(self.countbluecells):
            self.cellID = int(self.bluecells[i])
            for x in range(self.n):
                if self.cellID%self.nx >= x*self.nx/self.n:
                    if self.cellID%self.nx < (x+1)*self.nx/self.n:
                        self.freq[x] += 1
        for x in range(self.n):
            if self.countbluecells > 0:
                self.freq[x] = self.freq[x]/self.countbluecells
        "calculate the corresponding entropy"
        self.s = 0
        for x in range(self.n):
            if self.freq[x] > 0:
                self.s += -self.freq[x]*log(self.freq[x])
        "plot a dot at the corresponding location"
        if self.count%1 == 0:
            if self.resetcount == 0:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="green")
            if self.resetcount == 1:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="red")
            if self.resetcount == 2:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="blue")
            if self.resetcount == 3:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="yellow")
            if self.resetcount == 4:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="purple")
            if self.resetcount == 5:
                self.z.delete(self.initialcircle)
                self.initialcircle = self.z.create_oval(25,243-75*(self.s),30,248-75*(self.s), fill="orange")
        self.entropycounter.config(text = self.s)
        
        
    def update_entropy(self):
        "create an horizontal frequency by splitting the x-axis in n parts"
        self.n = self.ScaleNslices.get()
        self.freq = [0 for x in range(self.n)]
        for x in range(self.n):
            self.freq[x] = 0 
        "calculate the frequency to be in one half of the box"
        for i in range(self.countbluecells):
            self.cellID = int(self.bluecells[i])
            for x in range(self.n):
                if self.cellID%self.nx >= x*self.nx/self.n:
                    if self.cellID%self.nx < (x+1)*self.nx/self.n:
                        self.freq[x] += 1
        for x in range(self.n):
            self.freq[x] = self.freq[x]/self.countbluecells
        self.count += 1
        "calculate the corresponding entropy"
        self.s = 0
        for x in range(self.n):
            if self.freq[x] > 0:
                self.s += -self.freq[x]*log(self.freq[x])
        "plot a dot at the corresponding location"
        if self.count%60 == 1:
            if self.resetcount == 0:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="green")
            if self.resetcount == 1:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="red")
            if self.resetcount == 2:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="blue")
            if self.resetcount == 3:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="yellow")
            if self.resetcount == 4:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="purple")
            if self.resetcount == 5:
                self.circle = self.z.create_oval(25+(self.count*1.)%600,243-75*(self.s),30+(self.count*1.)%600,248-75*(self.s), fill="orange")
        self.entropycounter.config(text = self.s)
        
    def run_kawazaki(self):
        self.beta = -self.ScaleEnergy.get()/self.epsilon
        if self.do_run:
            self.simuspeed = pow(2,self.ScaleSpeed.get())
            self.Nfreebuffer = 0 #initiates the number of free particles to zero
            self.Emeanbuffer =0 #initiates the average energy to zero
            self.E2buffer = 0 #initiates the average energy squared to zero
            self.statcount = 0
            for i in range(self.simuspeed):
                self.MC_kawazaki_step()
                if i%self.countbluecells == 0:
                    self.statcount += 1
                    #let us count how many free particles there are
                    self.Nfreebuffer += self.Count_Nfree()
                    #and now take care of the energy
                    self.Emeanbuffer += self.energy
                    self.E2buffer += self.energy*self.energy
            #now let us devide by the number of samples...only valid when it is big enough otherwise correlations possible
            if self.statcount > 0:
                self.Nfreebuffer = self.Nfreebuffer/self.statcount
                self.Nfree += self.Nfreebuffer#statistics accumulation
                self.Emeanbuffer = self.Emeanbuffer/self.statcount
                self.Emean += self.Emeanbuffer#statistics accumulation
                self.E2buffer = self.E2buffer/self.statcount
                self.E2 += self.E2buffer
                self.CountForAverage += 1#statistics accumulation
            #and finally let us update the graphical output
            #self.update_canvas()
            self.update_canvas()
            self.update_rect()
            self.update_entropy()
            self.update_energy()
        self.after(1, self.run_kawazaki)           
    
    def PB_Left(self, x, a):
        if x%a == 0:
            return (x-1+a)%self.ntot
        else:
            return (x-1)%self.ntot
            
    def Count_Nfree(self):
        y = 0
        for x in range(self.countbluecells):
            self.cellIDbis = int(self.bluecells[x])
            if self.Count_Neighbours(self.cellIDbis) == 0:
                y += 1
        return y
    
    def PB_Right(self, x, a):
        if (x+1)%a == 0:
            return (x+1-a)%self.ntot
        else: 
            return (x+1)%self.ntot
    
    def Count_Neighbours(self, x):
        self.countneighbours = 0 
        if self.state[self.PB_Right(x, self.nx)] == 1: #right neighbour
            self.countneighbours += 1
        if self.state[self.PB_Left(x, self.nx)] == 1: #left neighbour
            self.countneighbours += 1
        if self.state[(x-self.nx)%self.ntot] == 1: #top neighbour
            self.countneighbours += 1
        if self.state[(x+self.nx)%self.ntot] == 1: #bottom neighbour
            self.countneighbours += 1
        return self.countneighbours
        
        
    def MC_kawazaki_step(self):
        self.Elost = 0
        self.Egained = 0
        self.cellRank = randint(1,self.countbluecells)
        self.cellID = int(self.bluecells[self.cellRank-1])
        self.direction = randint(1,4) #1=right, 2=left, 3=up, 4=down
        if self.direction == 1:
            self.Dest = self.PB_Right(self.cellID, self.nx)
        if self.direction == 2:
            self.Dest = self.PB_Left(self.cellID, self.nx)
        if self.direction == 3:
            self.Dest = self.cellID-self.nx
        if self.direction == 4:
            self.Dest = (self.cellID+self.nx)%self.ntot
        if self.state[self.Dest] == 0: #destination cell unoccupied
            "Energy that will potentially be lost via the move"
            self.Elost = self.epsilon*self.Count_Neighbours(self.cellID)
            "Energy that will potentially be gained via the move"
            self.Egained = self.epsilon*(self.Count_Neighbours(self.Dest)-1) #-1 because this cell will always cellID as neighbour
            self.r = random()
            self.DE = self.Elost - self.Egained
            if self.r < exp(self.beta*self.DE):
                self.bluecells.append(self.canvas.gettags(self.rect[self.Dest])[0])
                self.state[self.Dest] = 1
                self.bluecells.remove(self.canvas.gettags(self.rect[self.cellID])[0])
                self.state[self.cellID] = 0
                self.energy = self.energy -self.DE
        
    def update_canvas(self):
        for i in range(self.ntot):
            if self.state[i] == 0:
                self.canvas.itemconfig(self.rect[i], state = NORMAL)
                self.canvas.itemconfig(self.rect[i+self.ntot], state = HIDDEN)
            if self.state[i] == 1:
                self.canvas.itemconfig(self.rect[i], state = HIDDEN)
                self.canvas.itemconfig(self.rect[i+self.ntot], state = NORMAL)
        self.canvas.update_idletasks()
                
        
    def update_rect(self):
        for x in range(self.nrect):
            self.w.delete(self.FreqRect[x])
        self.nrect = self.ScaleNslices.get()
        self.Delta = 400*3/(4*self.nrect-1)
        self.T = 4*self.Delta/3
        self.freq = [0 for x in range(self.nrect)]
        for x in range(self.nrect):
            self.freq[x] = 0 
        "calculate the frequency to be in one half of the box"
        for i in range(self.countbluecells):
            self.cellID = int(self.bluecells[i])
            for x in range(self.nrect):
                if self.cellID%self.nx >= x*self.nx/self.nrect:
                    if self.cellID%self.nx < (x+1)*self.nx/self.nrect:
                        self.freq[x] += 1
        self.FreqRect = [0 for x in range(self.nrect)]
        for x in range(self.nrect):
            if self.countbluecells > 0:
                self.freq[x] = self.freq[x]/self.countbluecells
            self.FreqRect[x] = self.w.create_rectangle(130+x*self.T-self.Delta*0.5, (1-self.freq[x])*100+70, 130+x*self.T+self.Delta*0.5, 170, fill="yellow")
    
    def compute_energy(self):
        self.epsilon = -20#-round(self.ScaleEnergy.get()/self.beta)
        self.energy = 0
        for i in range(self.countbluecells):
            self.cellIDi = int(self.bluecells[i])
            if (self.cellIDi+1)%self.nx == 0:#check that it is on the right border
                for j in range(i,self.countbluecells):
                    self.cellIDj = int(self.bluecells[j])
                    if abs(self.cellIDj-self.cellIDi)==self.nx-1:
                        self.energy += self.epsilon
                    if abs(self.cellIDj-self.cellIDi)== self.nx:
                        self.energy += self.epsilon
            if (self.cellIDi+1)%self.nx != 0:#check that it is on the right border
                for j in range(i,self.countbluecells):
                    self.cellIDj = int(self.bluecells[j])
                    if abs(self.cellIDj-self.cellIDi)==1:
                        self.energy += self.epsilon
                    if abs(self.cellIDj-self.cellIDi)== self.nx:
                        self.energy += self.epsilon
            
        
root = Tk()
root.title("Fluid simulation")
root.geometry("1500x900")

app = Application(root)

root.mainloop()
        