from tkinter import *
from numpy import *


class Application(Frame):
    "A GUI application with 1 click-counting button"
    def __init__(self,master):
        "Initialize the Frame"
        Frame.__init__(self,master)
        self.grid()
        self.count = 0
        self.create_widgets()
    
    def create_widgets(self):
        "create a scale button to choose number of columns from 1 to 100"
        self.ScaleColumnVar = IntVar
        self.ScaleColumns = Scale(self, from_=0, to =100, orient = HORIZONTAL, label = "Columns", variable = self.ScaleColumnVar, font =('Helvetica','18'))
        self.ScaleColumns.grid()
        self.ScaleColumns.set(4)
        "create a scale button to choose number of rows from 1 to 100"
        self.ScaleRowVar = IntVar
        self.ScaleRows = Scale(self, from_=0, to =100, orient = HORIZONTAL, label = "Rows", variable = self.ScaleRowVar, font =('Helvetica','18'))
        self.ScaleRows.grid()
        self.ScaleRows.set(4)
        "create an array of dice all initiated with one spot facing up"
        self.ncolumns = 4
        self.nrow = 4
        self.ndice = self.ncolumns*self.nrow
        self.dice = [0 for x in range(self.ndice)]
        for x in range(self.ndice):
            self.dice[x] = 1
        "create the frequency of a given value and initiate it at 1 for 1 and zero otherwise"
        self.freq = [0 for x in range(6)]
        for x in range(6):
            self.freq[x] = 0
        self.freq[0] = 1
        "create an array of rectangles framed by axis in a canvas"
        self.w = Canvas(self, width = 300, height = 150)
        self.w.grid()
        self.line1 = self.w.create_line(8.,0,8.,150, arrow=FIRST, fill="black", width = 3)
        self.line2 = self.w.create_line(2.,145,300,145, arrow=LAST, fill="black", width = 3)
        self.w.create_text(60,15, text = "Frequency", font =('Purisa','18'))
        self.w.create_text(270,120, text = "Sides", font =('Purisa','18'))
        self.rect = [0 for x in range(6)]
        for x in range(6):
            self.rect[x] = self.w.create_rectangle(35+x*40-8, (1-self.freq[x])*100+50, 35+x*40+8, 150, fill="blue")
        "create an image of six dice below the histogram"
        self.sixdiceimage = PhotoImage(file="sixdice.gif")
        self.x = Canvas(self, width = 300, height = 40)
        self.x.grid()
        self.x.sixdice = self.x.create_image(146,20, image = self.sixdiceimage)
        "create a circle in a canvas to plot the entropy as a function of time"
        self.z = Canvas(self, width = 700, height = 200)
        self.z.grid()
        self.line3 = self.z.create_line(10.,0,10.,150, arrow=FIRST, fill="black", width = 3)
        self.line4 = self.z.create_line(2.,145,700,145, arrow=LAST, fill="black", width = 3)
        self.z.create_text(60,15, text = "Entropy", font =('Helvetica','18'))
        self.z.create_text(650,170, text = "Time", font =('Helvetica','18'))
        self.circle = self.z.create_oval(25,195-50,30,200-50, fill="green")
        "create a start/stop button"
        self.do_run = False
        self.startclick = True
        self.buttonStartStop = Button(self, text = "Start/Stop", font =('Helvetica','18'))
        self.buttonStartStop["command"] = self.start_stop_simu
        self.buttonStartStop.grid()
        "create a reset button"
        self.resetcount = 0
        self.Reset = Button(self, text = "Reset", font =('Helvetica','18'))
        self.Reset["command"] = self.reset_simu #the option "command" means action when clicking on it
        self.Reset.grid()
        "create 6 images"
        self.image = [0 for x in range(6)]
        for x in range(6):
            y = x+1
            filename = "dice" + str(y) + ".gif"
            self.image[x] = PhotoImage(file=filename)
        "create 6 layers of an array of dice"
        self.btn =  [[0 for x in range(self.ndice)] for n in range(6)]
        for n in range(6):
            for x in range(self.ndice):
                self.btn[n][x] = Button(self, image = self.image[n], compound=BOTTOM, width=50, height=50)
                self.btn[n][x].grid(row = 1+x//self.ncolumns, column = 3+x%self.ncolumns)
                if n > 0:
                    self.btn[n][x].grid_remove()

        def start_stop_simu(self):
            if self.startclick:
                self.start_simu()
                self.startclick = False
            else :
                self.stop_simu()
                self.startclick = True

        def start_simu(self):
            self.do_run = True
            self.run_simu()
    
    
        def run_simu(self):
            if self.do_run:
                if self.ndice > 50:
                    self.speed = 20
                else:
                    self.speed = 1
                for n in range(self.speed):
                    "pick a die at random and move it random to either nearest number"
                    xd = random.random_integers(0,self.ndice-1)
                    self.randstep = 2*random.random_integers(0,1)-1
                    if self.dice[xd]>1:
                        if self.dice[xd] == 6:
                            if self.randstep == 1:
                                self.dicetemp = 1
                            else:
                                self.dicetemp = 5
                        else:
                            self.dicetemp = self.dice[xd]+self.randstep
                    if self.dice[xd]==1:
                        if self.randstep == -1:
                            self.dicetemp = 6
                        else :
                            self.dicetemp = 2
                    "update the image"
                    self.btn[self.dice[xd]-1][xd].grid_remove()
                    self.btn[self.dicetemp-1][xd].grid()
                    self.dice[xd]=self.dicetemp
                    "update the frequency function"
                    for x in range(6):
                        self.freq[x] = 0
                        for y in range(self.ndice):
                            if self.dice[y] == (x+1):
                                self.freq[x] += 1
                        self.freq[x]/=self.ndice
                    "update the canvas histogram"
                    for x in range(6):
                        self.w.delete(self.rect[x])
                        self.rect[x] = self.w.create_rectangle(35+x*40-8, (1-self.freq[x])*100+50, 35+x*40+8, 150, fill="blue")
                    "update the entropy canvas"
                    self.count += 1
                    self.s = 0
                    for x in range(6):
                        if self.freq[x] > 0:
                            self.s += -self.freq[x]*log(self.freq[x])
                    if self.count%3 == 1:
                        if self.resetcount == 0:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="green")
                        if self.resetcount == 1:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="red")
                        if self.resetcount == 2:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="blue")
                        if self.resetcount == 3:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="yellow")
                        if self.resetcount == 4:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="purple")
                        if self.resetcount == 5:
                            self.circle = self.z.create_oval(25+self.count*1.,-50+195-50*(self.s),30+self.count*1.,-50+200-50*(self.s), fill="orange")
                self.after(100, self.run_simu)

        def stop_simu(self):
            self.do_run = False
    
        def reset_simu(self):
            self.stop_simu()
            self.count = 0
            self.resetcount = (self.resetcount+1)%6
            "delete the array of dice images"
            del self.dice[:]
            for n in range(6):
                for x in range(self.ndice):
                    self.btn[n][x].destroy()
    
            "create an array of dice all initiated with one spot facing up"
            self.ncolumns = self.ScaleColumns.get()
            self.nrow = self.ScaleRows.get()
            self.ndice = self.ncolumns*self.nrow
            self.dice = [0 for x in range(self.ndice)]
            for x in range(self.ndice):
                self.dice[x] = 1
            "reset the frequency function"
            for x in range(6):
                self.freq[x]=0
            self.freq[0] = 1
            "update the canvas histogram"
            for x in range(6):
                self.w.delete(self.rect[x])
                self.rect[x] = self.w.create_rectangle(35+x*40-8, (1-self.freq[x])*100+50, 35+x*40+8, 150, fill="blue")
            "re-create 6 layers of an array of dice"
            self.btn =  [[0 for x in range(self.ndice)] for n in range(6)]
            for n in range(6):
                for x in range(self.ndice):
                    self.btn[n][x] = Button(self, image = self.image[n], compound=BOTTOM, width=50, height=50)
                    self.btn[n][x].grid(row = 1+x//self.ncolumns, column = 5+x%self.ncolumns)
                    if n > 0:
                        self.btn[n][x].grid_remove()


root = Tk()
root.title("Dice dynamics")
root.geometry("1400x700")

app = Application(root)

root.mainloop()
